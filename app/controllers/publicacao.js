// Vai ser utilizado na próxima atividade
// var lista = require('../models/publicacao');

module.exports = function() 
{
    var controller = {};
    var lista = [
        {_id:'2001',texto:'Mensagem 1'},
        {_id:'2002',texto:'Mensagem 2'},
        {_id:'2003',texto:'Mensagem 3'}
    ];


    controller.index = function(req,res) {
        res.json(lista);
    };

    controller.getById = function(req,res) {
        var id = req.params.id;
        var pub = lista.find((p)=>(p._id==id));
        pub ? 
            res.json(pub) :
            res.status(404).send('Publicação nao existe');
    };

    controller.save = function(req,res,next) {
        var item = req.body;
            item._id = 2001 + lista.length;

        var posicao = lista.push(item);
        (posicao>=0) ? 
            res.json(item) :
            res.status(404).send("Não foi possível gravar sua publicação");
    };

    controller.update = function(req,res) {
        var item = req.body,
            idProcurado = item._id;

        var posicao = lista.find(function(obj,indice){
            if(obj._id===idProcurado) {
                return indice;
            }
        });

        lista[posicao] = item;
        res.json(lista);
    };

    controller.delete = function(req,res) {
        var remover = req.params.id;
        var tmp = lista.filter(function(a,b){
            return (a._id !== remover);
        });

        lista = tmp;

        res.status(200).send('Publicação removida');
    };

    controller.getUser = function(req,res) {
        res.json(lista);
    };


    return controller;
}