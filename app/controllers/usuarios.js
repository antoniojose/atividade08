// Vai ser utilizado na próxima atividade
// var lista = require('../models/usuarios');

module.exports = function() 
{
    var controller = {};
    var lista = [
        {_id:'1001',nome:'João',email:'joao@up.co',senha:'qwe123'},
        {_id:'1002',nome:'Maria',email:'maria@up.co',senha:'qwe123'},
        {_id:'1003',nome:'Pedro',email:'pedro@up.co',senha:'qwe123'}
    ];

    controller.index = function(req,res) {
        res.json(lista);
    };


    controller.getById = function(req,res) {
        var id = req.params.id;
        var usuario = lista.find((user)=>(user._id==id));
        usuario ? 
            res.json(usuario) :
            res.status(404).send('Usuário nao existe');
    };


    controller.save = function(req,res) {
        var item = req.body;
            item._id = 1001 + lista.length;

        var posicao = lista.push(item);
        (posicao>=0) ? 
            res.json(item) :
            res.status(404).send("Não foi possível gravar este usuário");
    };

    controller.update = function(req,res) {

        var item = req.body,
            idProcurado = item._id;

        var posicao = lista.find(function(obj,indice){
            if(obj._id===idProcurado) {
                return indice;
            }
        });

        lista[posicao] = item;

        res.json(lista);
    };

    controller.delete = function(req,res) {
        var remover = req.params.id;
        var tmp = lista.filter(function(a,b){
            return (a._id !== remover);
        });

        lista = tmp;

        res.status(200).send('Usuário removido');
    };

    controller.getByUser = function(req,res) {
        res.json(lista);
    };


    return controller;
}