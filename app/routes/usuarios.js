var express = require('express');
var controller = require('../controllers/usuarios')();
var router = express.Router();


router.get('/usuarios', controller.index);
router.get('/usuarios/:id', controller.getById);
router.post('/usuarios', controller.save);
router.put('/usuarios/:id', controller.update);
router.delete('/usuarios/:id', controller.delete);
router.get('/usuarios/:id/posts', controller.getByUser);

module.exports = router; 
