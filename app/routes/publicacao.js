var express = require('express');
var controller = require('../controllers/publicacao')();
var router = express.Router();

router.get('/posts', controller.index); 
router.get('/posts/:id', controller.getById);
router.post('/posts', controller.save);
router.put('/posts/:id',controller.update);
router.delete('/posts/:id', controller.delete);
router.get('/posts/:id/usuario',controller.getUser);

module.exports = router;
