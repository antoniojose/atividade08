var express = require('express');
var rotasUsuario = require('../app/routes/usuarios');
var rotasPublicacao = require('../app/routes/publicacao');



module.exports = function() {
    
    var app = express();

    app.set('port',3000);
    app.use(express.static('./public'));
    
    app.use('/api/u/', rotasUsuario);
    app.use('/api/p/', rotasPublicacao); 
    
    return app; 
}